app.controller('AnonymizationServerCtrl', function ($scope, config, AnonymizationIoc) {
    var dimension1 = config.dimension1;
    var dimension2 = config.dimension2;

    $scope.dimension1 = dimension1;
    $scope.dimension2 = dimension2;

    var iocs = [];

    function init() {
        angular.forEach(dimension1, function (rowValue, rowIndex) {
            var iocRow = [];
            angular.forEach(dimension2, function (columnValue, columnIndex) {
                var ioc = new AnonymizationIoc(dimension1, dimension2, rowIndex, columnIndex);
                iocRow[columnValue] = ioc;
            });
            iocs[rowValue] = iocRow;
        });
    }

    init();

    $scope.getValue1 = function (i, j, iocI) {
        return iocs[i][j].getValue1(iocI);
    };

    $scope.getValue2 = function (i, j, iocJ) {
        return iocs[i][j].getValue2(iocJ);
    };
});