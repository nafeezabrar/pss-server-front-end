function AnonymizationIoc(dimension1, dimension2, rowIndex, columnIndex) {
    var iocRow1 = [];
    var iocRow2 = [];


    angular.forEach(dimension1, function (value, index) {
        if (index == rowIndex) {
            iocRow1[value] = {value: ''}
        } else {
            iocRow1[value] = {value: 0};
        }
    });

    angular.forEach(dimension2, function (value, index) {
        if (index == columnIndex) {
            iocRow2[value] = {value: ''};
        }
        else {
            iocRow2[value] = {value: 0};
        }
    });

    this.getValue1 = function (iocI) {
        return iocRow1[iocI].value;
    };

    this.getValue2 = function (iocJ) {
        return iocRow2[iocJ].value;
    };
}

app.factory('AnonymizationIoc', function () {
    return AnonymizationIoc;
});